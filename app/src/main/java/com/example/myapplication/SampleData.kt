/**
 * SampleData for Jetpack Compose Tutorial 
 */
object SampleData {
    // Sample conversation data
    val conversationSample = listOf(
        Message(
            "Klaudia",
            "Plamka is jumping on the door"
        ),
        Message(
            "Klaudia",
            """My current wishlist:
                |Rarebeauty blush
                |Rhode lipbalm
                |Face masks
                |Serum from Estee Lauder 
                |Face snail essence
                |Face foam from Garnier
            """.trimMargin().trim()
        ),
        Message(
            "Klaudia",
            """Bro I slept 5 hours today""".trim()
        ),
        Message(
            "Klaudia",
            "The deadline for application submission is the 16th of January 2024 (included). The selection results will be published at the latest on the 20th of January"
        ),
        Message(
            "Klaudia",
            """Why are you always taking so long to reply?""".trim()
        ),
        Message(
            "Klaudia",
            "I'll be back in 15 minutes, where are you?"
        ),
        Message(
            "Klaudia",
            """Are you coming to the prom in October?
                |I told them that you are so I hope you can come
            """.trim()
        ),
        Message(
            "Klaudia",
            "Look at this Michael Jackson song, it's so nice"
        ),
        Message(
            "Klaudia",
            "Wanna order pizza? I'm starving"
        ),
        Message(
            "Klaudia",
            "Come outside we'll make a snowman"
        ),
        Message(
            "Klaudia",
            "Look at this new cafe I really want to go there"
        ),
        Message(
            "Klaudia",
            "Can we have a British shorthair when we're older?"
        ),
        Message(
            "Klaudia",
            "jprdl"
        ),
    )
}
